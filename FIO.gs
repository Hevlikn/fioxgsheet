var FIO_URL = "https://rest.fnar.net";

function hasAuthUserPropertyKeys()
{
  var userProperties = PropertiesService.getUserProperties();
  return userProperties.getKeys().indexOf("UserName") >= 0 && userProperties.getKeys().indexOf("Password") >= 0;
}

function tryLoginCached()
{
  var userProperties = PropertiesService.getUserProperties();
  if (hasAuthUserPropertyKeys())
  {
    var username = userProperties.getProperty("UserName");
    var password = userProperties.getProperty("Password");
    return tryLogin(username, password);
  }
  
  return false;
}

function tryLogin(username, password)
{
  var authPayload = new Object();
  authPayload["UserName"] = username;
  authPayload["Password"] = password;
      
  var postOptions = new Object();
  postOptions["method"] = "POST";
  postOptions["contentType"] = "application/json";
  postOptions["payload"] = JSON.stringify(authPayload);
  postOptions["muteHttpExceptions"] = true;
      
  var authResponse = UrlFetchApp.fetch(FIO_URL + "/auth/login", postOptions);
  if ( authResponse.getResponseCode() == 200 )
  {
    var authObj = JSON.parse(authResponse.getContentText());
    if (authObj.AuthToken)
    {
      var userProperties = PropertiesService.getUserProperties();
      userProperties.setProperty("UserName", username);
      userProperties.setProperty("Password", password);
      userProperties.setProperty("Authentication", authObj.AuthToken);
      return true;
    }
  }

  return false;
}

function login()
{
  var ui = SpreadsheetApp.getUi();
  
  if (tryLoginCached())
  {
    return true;
  }
  
  var username = "";
  var password = "";
  
  var result = ui.prompt('Login', 'UserName:', ui.ButtonSet.OK_CANCEL);
  var button = result.getSelectedButton();
  var text = result.getResponseText();
  if ( button == ui.Button.OK )
  {
    username = text;
    
    result = ui.prompt('Password', 'Password:', ui.ButtonSet.OK_CANCEL);
    button = result.getSelectedButton();
    text = result.getResponseText();
    if ( button == ui.Button.OK )
    {
      password = text;
      
      if ( tryLogin(username, password) )
      {
        ui.alert("Login Successful.");
        return true;
      }
      else
      {
        ui.alert("Login Failed.");
        return false;
      }
    }
  }
  
  return false;
}

function FIOInternal_GET(endpoint)
{
  var userProperties = PropertiesService.getUserProperties();
  if ( userProperties.getKeys().indexOf("Authentication") )
  {
    var auth = userProperties.getProperty("Authentication");
    var header = {headers: {Authorization: auth}, muteHttpExceptions: true};
    
    var jsondata = UrlFetchApp.fetch(FIO_URL + endpoint, header);
    if ( jsondata.getResponseCode() == 200 )
    {
      var object = JSON.parse(jsondata.getContentText());
      return object;
    }
  }
  
  return null;
}

function FIO_PopulateSheet(sheetName, headersArray, endpoint)
{
  var sheet = getAndClearOrCreateSheet(sheetName);
  
  var newData = [];
  newData.push(headersArray);
  
  var jsonData = FIOInternal_GET(endpoint);
  if (jsonData != null )
  {
    for( var i = 0; i < jsonData.length; i++)
    {
      var row = jsonData[i];
      var rowData = []
      for ( var headerIdx = 0; headerIdx < headersArray.length; headerIdx++ )
      {
        var header = headersArray[headerIdx];
        rowData.push(row[header]);
      }
        
      newData.push(rowData);
    }
  }
  
  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  outputRange.setValues(newData);
}

function FIO_SetStatus(newStatus)
{
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var inputSheet = app.getSheetByName("Input");
  var g4Range = inputSheet.getRange("G4");
  g4Range.setValue(newStatus);
  SpreadsheetApp.flush();
}

function FIO_Import()
{
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var inputSheet = app.getSheetByName("Input");
  var g4Contents = inputSheet.getRange("G4").getValue();
  var userName = null;
  if (g4Contents != null & g4Contents.length > 2)
  {
    userName = g4Contents;
  }
  
  FIO_SetStatus("FIO: Authenticating...");
  
  var bLoggedIn = tryLoginCached();
  if(!bLoggedIn)
  {
    bLoggedIn = login();
  }

  if (bLoggedIn)
  {
    if (userName == null)
    {
      var userProperties = PropertiesService.getUserProperties();
      var userName = userProperties.getProperty("UserName");  
    }
  
    if (userName != null && userName.length > 0)
    {
      FIO_SetStatus("FIO: (1/10) Retrieving Financial Data...");
      FIO_PopulateSheet("DI-Liquid", ["Source", "Currency", "Amount"], "/rain/userliquid/" + userName);
      
      FIO_SetStatus("FIO: (2/10) Retrieving Planet Data...");
      FIO_PopulateSheet("DI-Planet", ["NaturalId", "Name"], "/rain/userplanets/" + userName);
      
      FIO_SetStatus("FIO: (3/10) Retrieving Building Data...");
      FIO_PopulateSheet("DI-PlanetBuildings", ["NaturalId", "Id", "Ticker", "Created", "Condition"], "/rain/userplanetbuildings/" + userName);
      
      FIO_SetStatus("FIO: (4/10) Retrieving Building Reclaimables Data...");
      FIO_PopulateSheet("DI-PlanetBuildingReclaimables", ["BuildingId", "Material", "Amount"], "/rain/userplanetbuildingreclaimables/" + userName);
      
      FIO_SetStatus("FIO: (5/10) Retrieving Production Data...");
      FIO_PopulateSheet("DI-PlanetProduction", ["NaturalId", "Type", "Id", "Completed", "Remaining"], "/rain/userplanetproduction/" + userName);
      
      FIO_SetStatus("FIO: (6/10) Retrieving Production Input Data...");
      FIO_PopulateSheet("DI-PlanetProductionInput", ["OrderId", "Material", "Count"], "/rain/userplanetproductioninput/" + userName);
      
      FIO_SetStatus("FIO: (7/10) Retrieving Production Output Data...");
      FIO_PopulateSheet("DI-PlanetProductionOutput", ["OrderId", "Material", "Count"], "/rain/userplanetproductionoutput/" + userName);
      
      FIO_SetStatus("FIO: (8/10) Retrieving Workforce Data...");
      FIO_PopulateSheet("DI-PlanetWorkforce", ["NaturalId", "Workforce", "Population", "Capacity", "Required", "Satisfaction"], "/rain/userplanetworkforce/" + userName);
      
      FIO_SetStatus("FIO: (9/10) Retrieving Storage Data...");
      FIO_PopulateSheet("DI-Storage", ["NaturalId", "Name", "Type", "Ticker", "Amount"], "/rain/userstorage/" + userName);
      
      FIO_SetStatus("");
    }
  }
}
