function getAndClearOrCreateSheet(sheetName) {
  var app = SpreadsheetApp.getActiveSpreadsheet()
  var dataImport = app.getSheetByName(sheetName);
  if (dataImport == null) {
    dataImport = app.insertSheet(sheetName);
  }
  
  dataImport.clear();
  return dataImport;
}

function onEdit(e) {
  console.log('Results changed, processing new results', e.range.getColumn(), e.range.getRow(), e.range.getLastColumn(), e.range.getLastRow());
  var sheet = e.range.getSheet();
  if (sheet.getName() == "Input") {
    copyAuditRange = sheet.getRange("Input!D5:D");
    // Treat copyAuditRange as a single column for simple math purposes
    if (e.range.getColumn() == copyAuditRange.getColumn()) {
      if (e.range.getRow() >= copyAuditRange.getRow() || e.range.getRow() <= copyAuditRange.getLastRow())
      {
        onInputChanged(sheet);
      }
    }
  }
}
function onInputChanged(sheet) {
  console.log('Results changed, processing new results');
  var inputs = sheet.getRange("Input!D5:D").getValues();
  var foundSomething = false;
  
  var jsonDataSet = [];
  for each (var row in inputs) {
    for each (var cell in row) {
      if (cell.length > 0) {
        foundSomething = true;
        
        var decoded = Utilities.base64Decode(cell);
        var asString = Utilities.newBlob(decoded).getDataAsString()
        var asJson = JSON.parse(asString);
        jsonDataSet.push(asJson);
      }
    }
  }
  
  console.log("Found ", jsonDataSet.length, " pieces of data to parse.");
  
  if (!foundSomething) {
    // Do nothing, clear it
    getAndClearOrCreateSheet("DI-Liquid");
    getAndClearOrCreateSheet("DI-Planet");
    getAndClearOrCreateSheet("DI-PlanetBuildings");
    getAndClearOrCreateSheet("DI-PlanetBuildingReclaimables");
    getAndClearOrCreateSheet("DI-PlanetProduction");
    getAndClearOrCreateSheet("DI-PlanetProductionInput");
    getAndClearOrCreateSheet("DI-PlanetProductionOutput");
    getAndClearOrCreateSheet("DI-PlanetWorkforce");
    getAndClearOrCreateSheet("DI-Storage");
  } else {
    importLiquidData(jsonDataSet);
    importPlanetData(jsonDataSet);
    importPlanetBuildingData(jsonDataSet);
    importPlanetBuildingReclaimablesData(jsonDataSet);
    importPlanetProductionData(jsonDataSet);
    importPlanetProductionInputData(jsonDataSet);
    importPlanetProductionOutputData(jsonDataSet);
    importPlanetWorkforceData(jsonDataSet);
    importStorageData(jsonDataSet);
  }
}

function importLiquidData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-Liquid");
  var newData = [];
  newData.push(["Source", "Currency", "Amount"]); 
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.liquid) {
      for each (var cxBuy in jsonData.liquid.cxBuys) {
        newData.push([cxBuy.naturalId, cxBuy.currency, cxBuy.amount]);
      }
      for each (var currency in jsonData.liquid.currency) {
        newData.push([currency.naturalId, currency.currency, currency.amount]);
      }
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-Planet");
  var newData = [];
  newData.push(["NaturalId", "Name"]); 
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      newData.push([jsonData.colony.naturalId, jsonData.colony.name]);
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetWorkforceData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetWorkforce");
  var newData = [];
  newData.push(["NaturalId", "Workforce", "Population", "Capacity", "Required", "Satisfaction"]);
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      newData.push([jsonData.colony.naturalId, "PIONEER", jsonData.colony.workforces.p.p, jsonData.colony.workforces.p.c, jsonData.colony.workforces.p.r, jsonData.colony.workforces.p.s]);
      newData.push([jsonData.colony.naturalId, "SETTLER", jsonData.colony.workforces.s.p, jsonData.colony.workforces.s.c, jsonData.colony.workforces.s.r, jsonData.colony.workforces.s.s]);
      newData.push([jsonData.colony.naturalId, "TECHNICIAN", jsonData.colony.workforces.t.p, jsonData.colony.workforces.t.c, jsonData.colony.workforces.t.r, jsonData.colony.workforces.t.s]);
      newData.push([jsonData.colony.naturalId, "ENGINEER", jsonData.colony.workforces.e.p, jsonData.colony.workforces.e.c, jsonData.colony.workforces.e.r, jsonData.colony.workforces.e.s]);
      newData.push([jsonData.colony.naturalId, "SCIENTIST", jsonData.colony.workforces.c.p, jsonData.colony.workforces.c.c, jsonData.colony.workforces.c.r, jsonData.colony.workforces.c.s]);
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetBuildingData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetBuildings");
  var newData = [];
  newData.push(["NaturalId", "Id", "Ticker", "Created", "Condition"]); 
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var building in jsonData.colony.buildings) {
        newData.push([jsonData.colony.naturalId, building.id, building.ticker, building.created, building.condition]);
      }
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetBuildingReclaimablesData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetBuildingReclaimables");
  var newData = [];
  newData.push(["BuildingId", "Material", "Amount"]);
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var building in jsonData.colony.buildings) {
        for each (var reclaimable in building.reclaimables) {
          newData.push([building.id, reclaimable[0], reclaimable[1]]);
        }
      }
    }
  }
  
  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetProductionData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetProduction");
  var newData = [];
  newData.push(["NaturalId", "Type", "Id", "Completed", "Remaining"]); 
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var productionLine in jsonData.colony.productionLines) {
        for each (var order in productionLine.orders) {
          newData.push([jsonData.colony.naturalId, productionLine.type, order.id, order.completed, order.remaining]);
        }
      }
    }
  }
  
  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importPlanetProductionInputData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetProductionInput");
  var newData = [];
  newData.push(["OrderId", "Material", "Count"]); 
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var productionLine in jsonData.colony.productionLines) {
        for each (var order in productionLine.orders) {
          if (order.remaining > 0) {
            for each (var io in order.inputs) {
              newData.push([order.id, io[0], io[1]]);
            }
          }
        }
      }
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}
function importPlanetProductionOutputData(jsonDataSet) {   
  var sheet = getAndClearOrCreateSheet("DI-PlanetProductionOutput");
  var newData = [];
  newData.push(["OrderId", "Material", "Count"]);
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var productionLine in jsonData.colony.productionLines) {
        for each (var order in productionLine.orders) {
          if (order.remaining > 0) {
            for each (var io in order.outputs) {
              newData.push([order.id, io[0], io[1]]);
            }
          }
        }
      }
    }
  }

  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData);
}

function importStorageData(jsonDataSet) { 
  var sheet = getAndClearOrCreateSheet("DI-Storage");
  var newData = [];
  newData.push(["NaturalId", "Name", "Type", "Ticker", "Amount"]);
  
  for each (var jsonData in jsonDataSet) {
    if (jsonData.colony) {
      for each (var inv in jsonData.colony.storage) {
        newData.push([jsonData.colony.naturalId, jsonData.colony.name, "STORE", inv[0], inv[1]]);
      }
    }
    if (jsonData.trades) {
      for each (var trade in jsonData.trades) {
        for each (var inv in trade.inventory) {
          newData.push([trade.naturalId, trade.name, trade.type, inv[0], inv[1]]);
        }
      }
    }
    if (jsonData.ships) {
      for each (var ship in jsonData.ships) {
        for each (var inv in ship.inventory) {
          newData.push([ship.naturalId, ship.name, ship.type, inv[0], inv[1]]);
        }
      }
    }
  }
  
  setSheetSize(sheet, newData.length, newData[0].length);
  var outputRange = sheet.getRange(1,1,newData.length, newData[0].length)
  newData = SortData(newData);
  outputRange.setValues(newData); 
}

function setSheetSize(sheet, rows, columns) {
  // Always ensure we have the header and two additional rows for predefined ranges to not break
  if (rows <= 3) {
    rows = 3;
  }
  if (sheet.getMaxRows() > rows) {
    sheet.deleteRows(rows, sheet.getMaxRows() - rows);
  } else if (sheet.getMaxRows() < rows) {
    sheet.insertRows(sheet.getMaxRows(), rows - sheet.getMaxRows())
  }
  
  if (sheet.getMaxColumns() > columns) {
    sheet.deleteColumns(columns, sheet.getMaxColumns() - columns);
  } else if (sheet.getMaxColumns() < columns) {
    sheet.insertColumns(sheet.getMaxColumns(), columns - sheet.getMaxColumns())
  } 
}

function ARRAYMULT(arr1, count, blank) {
  var output = [];
  blank = blank || false;
  arr1.forEach(function(xx) {
    for (var i = 0; i < count; i++) {
      if (i > 0 && blank) {
        output.push("");
      } else {
        output.push(xx);
      }
    }
  });
  return output;
}


function ARRAYJOIN(arr1, arr2) {
  var output = [];
  arr1.forEach(function(xx) {
    arr2.forEach(function(yy) {
      output.push(xx.concat(yy));
    });
  });
  return output;
}

function ARRAYINNERJOIN(arr1, arr2, col1, col2) {
  var output = [];
  arr1.forEach(function(xx) {
    arr2.forEach(function(yy) {
      if (xx[col1] == yy[col2] && !!xx[col1]) {
        output.push(xx.concat(yy));
      }
    });
  });
  return output;
}

function SortData(values) {
  header = values[0];
  var data = values.slice(1);
  data.sort(function (a,b) {
    for (var ix = 0; ix < a.length; ix++ )
    {
      var diff = (a[ix] || '').toString().localeCompare(b[ix] || '');
      if (diff !== 0) {
        return diff;
      }
    }
    return 0;
  });
  return [header].concat(data);
}
